using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AngularTraining.Test.IoC;
using AngularTraining.WebAPI;
using System;
using StructureMap;
using System.Collections.Generic;
using System.Text;
using AngularTraining.Infrastructure.DAL.EF;
using AngularTraining.Core.Externals.Repositories;

namespace AngularTraining.Test.Base
{
    public class TestServerStartup : Startup
    {
        public TestServerStartup(IConfiguration configuration) : base(configuration)
        {
        }

        public override IServiceProvider ConfigureIoC(IServiceCollection services)
        {
            var container = StructureMapContainerInit.InitializeContainer();
            container.Inject(Configuration);
            container.Populate(services);
            return container.GetInstance<IServiceProvider>();
        }
    }
}