using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace AngularTraining.Infrastructure.Migrations
{
    public partial class AddClassScheduleTable2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ClassScheduleDBSet",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CreatedDate = table.Column<DateTimeOffset>(nullable: false),
                    UpdatedDate = table.Column<DateTimeOffset>(nullable: false),
                    RoomNumber = table.Column<string>(nullable: true),
                    StartTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    Lecture = table.Column<int>(nullable: false),
                    Lab = table.Column<int>(nullable: false),
                    Subject = table.Column<int>(nullable: false),
                    MidtermExam = table.Column<DateTime>(nullable: false),
                    FinalExam = table.Column<DateTime>(nullable: false),
                    Credit = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClassScheduleDBSet", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClassScheduleDBSet");
        }
    }
}