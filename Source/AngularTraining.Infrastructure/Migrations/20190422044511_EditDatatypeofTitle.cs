using Microsoft.EntityFrameworkCore.Migrations;

namespace AngularTraining.Infrastructure.Migrations
{
    public partial class EditDatatypeofTitle : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Subject",
                table: "ClassScheduleDBSet",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Subject",
                table: "ClassScheduleDBSet",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}