using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using AngularTraining.Core.DomainModels.Identities;
using AngularTraining.Core.Externals.Repositories;
using AngularTraining.Infrastructure.DAL.EF;
using System.Threading;
using System.Threading.Tasks;

namespace AngularTraining.Infrastructure.Identities
{
    public class ApplicationUserStoreDecorator : IUserStore<ApplicationUser>, IUserPasswordStore<ApplicationUser>, IUserEmailStore<ApplicationUser>
    {
        private UserStore<ApplicationUser> userStore;

        public ApplicationUserStoreDecorator(IUnitOfWork unitOfWork)
        {
            userStore = new UserStore<ApplicationUser>((UnitOfWork)unitOfWork);
        }

        public async Task<IdentityResult> CreateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.CreateAsync(user, cancellationToken);
        }

        public async Task<IdentityResult> DeleteAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.DeleteAsync(user, cancellationToken);
        }

        public async Task<ApplicationUser> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            return await userStore.FindByIdAsync(userId,cancellationToken);
        }

        public async Task<ApplicationUser> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            return await userStore.FindByNameAsync(normalizedUserName, cancellationToken);
        }

        public async Task<string> GetNormalizedUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.GetNormalizedUserNameAsync(user, cancellationToken);
        }

        public async Task<string> GetPasswordHashAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.GetPasswordHashAsync(user, cancellationToken);
        }

        public async Task<string> GetUserIdAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.GetUserIdAsync(user, cancellationToken);
        }

        public async Task<string> GetUserNameAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.GetUserNameAsync(user, cancellationToken);
        }

        public async Task<bool> HasPasswordAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.HasPasswordAsync(user, cancellationToken);
        }

        public async Task SetNormalizedUserNameAsync(ApplicationUser user, string normalizedName, CancellationToken cancellationToken)
        {
            await userStore.SetNormalizedUserNameAsync(user, normalizedName,cancellationToken);
        }

        public async Task SetPasswordHashAsync(ApplicationUser user, string passwordHash, CancellationToken cancellationToken)
        {
            await userStore.SetPasswordHashAsync(user, passwordHash, cancellationToken);
        }

        public async Task SetUserNameAsync(ApplicationUser user, string userName, CancellationToken cancellationToken)
        {
            await userStore.SetUserNameAsync(user, userName, cancellationToken);
        }

        public async Task<IdentityResult> UpdateAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.UpdateAsync(user, cancellationToken);
        }

        public async Task SetEmailAsync(ApplicationUser user, string email, CancellationToken cancellationToken)
        {
            await userStore.SetEmailAsync(user, email, cancellationToken);
        }

        public async Task<string> GetEmailAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.GetEmailAsync(user, cancellationToken);
        }

        public async Task<bool> GetEmailConfirmedAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.GetEmailConfirmedAsync(user, cancellationToken);
        }

        public async Task SetEmailConfirmedAsync(ApplicationUser user, bool confirmed, CancellationToken cancellationToken)
        {
            await userStore.SetEmailConfirmedAsync(user, confirmed, cancellationToken);
        }

        public async Task<ApplicationUser> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            var result =  await userStore.FindByEmailAsync(normalizedEmail, cancellationToken);
            return result;
        }

        public async Task<string> GetNormalizedEmailAsync(ApplicationUser user, CancellationToken cancellationToken)
        {
            return await userStore.GetNormalizedEmailAsync(user, cancellationToken);
        }

        public async Task SetNormalizedEmailAsync(ApplicationUser user, string normalizedEmail, CancellationToken cancellationToken)
        {
            await userStore.SetNormalizedEmailAsync(user, normalizedEmail, cancellationToken);
        }

        public void Dispose()
        {
            userStore.Dispose();
        }

    }
}