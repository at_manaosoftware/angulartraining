using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using AngularTraining.Core.DomainModels.Identities;
using AngularTraining.Core.Externals.Identities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Infrastructure.Identities
{
    public class ApplicationSignInManager : SignInManager<ApplicationUser>, IApplicationSignInManager
    {
        public ApplicationSignInManager(UserManager<ApplicationUser> userManager, IHttpContextAccessor contextAccessor, IUserClaimsPrincipalFactory<ApplicationUser> claimsFactory, IOptions<IdentityOptions> optionsAccessor, ILogger<SignInManager<ApplicationUser>> logger, IAuthenticationSchemeProvider schemes)
            : base(userManager, contextAccessor, claimsFactory, optionsAccessor, logger, schemes)
        {
        }
    }
}