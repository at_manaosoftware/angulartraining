using Microsoft.EntityFrameworkCore;
using AngularTraining.Core.DomainModels.Tenants;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Infrastructure.DAL.EF.Mappings
{
    public static class TenantMap
    {
        public static ModelBuilder MapTenant(this ModelBuilder modelBuilder)
        {
            var entity = modelBuilder.Entity<Tenant>();
            entity.ToTable("Tenants");
            entity.Property(c => c.Id).ValueGeneratedOnAdd();
            entity.HasMany(c => c.Users)
                .WithOne(c => c.Tenant)
                .HasForeignKey(x => x.TenantId)
                .OnDelete(DeleteBehavior.Cascade);
            entity.Property(c => c.CreatedDate);
            entity.Property(c => c.UpdatedDate);
            return modelBuilder;
        }
    }   
}