using AngularTraining.Core.Externals.Messages;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace AngularTraining.Infrastructure.Messages
{
    public class EmailService : IEmailSender
    {
        private IOptions<SendGridEmailSenderOptions> emailSenderOptions;

        public EmailService(IOptions<SendGridEmailSenderOptions> emailSenderOptions)
        {
            this.emailSenderOptions = emailSenderOptions;
        }
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var client = new SendGridClient(emailSenderOptions.Value.ApiKey);
            var msg = new SendGridMessage()
            {
                From = new EmailAddress(emailSenderOptions.Value.DefaultSenderEmail, emailSenderOptions.Value.DefaultSenderName),
                Subject = subject,
                HtmlContent = message
            };
            msg.AddTo(new EmailAddress(email));
            var response = await client.SendEmailAsync(msg);
            if (response.StatusCode != HttpStatusCode.Accepted)
            {
                var body = await response.Body.ReadAsStringAsync();
                throw new Exception("send grid error: " + Environment.NewLine + body);
            }
        }
    }
}