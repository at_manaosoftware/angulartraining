import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProfileRoutingModule } from './profile.routes';
import { ProfileComponent } from './components/profile/profile.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { CoreModule } from '../core/core.module';
import { ProfileLayoutComponent } from './components/profile-layout/profile-layout.component';

@NgModule({
    declarations: [
        ProfileComponent,
        ChangePasswordComponent,
        ProfileLayoutComponent
    ],
    imports: [
        CommonModule,
        ProfileRoutingModule,
        CoreModule,
    ],
    exports: []
})
export class ProfileModule { }
