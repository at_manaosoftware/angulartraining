import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/models/profiles/profile.model';
import { BasePageComponent } from 'src/app/modules/core/components/bases/base-page-component';
import { UserService } from 'src/app/modules/core/services/user.service';
import { Router } from '@angular/router';
import { SystemMessageService } from 'src/app/modules/core/services/system-message.service';
import { SystemMessageStatus } from 'src/app/models/system-messages/system-message-types';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent extends BasePageComponent implements OnInit {

    public model = new Profile();
    public disabled = false;
    constructor(public userService: UserService, private router: Router, private sm: SystemMessageService) {
        super(userService);
     }

    ngOnInit() {
        if (this.user) {
            this.model = new Profile(this.user);
        } else {
            this.router.navigate(['/account/login']);
        }
    }
    public save() {
        if (this.model.isModelValid()) {
            this.disabled = true;
            this.userService.updateUserProfile(this.model).then(() => {
                this.disabled = false;
                this.displaySuccessMessage();
            }).catch((err) => {
                this.disabled = false;
                throw err;
            });
        }
    }
    private displaySuccessMessage() {
        debugger;
        this.sm.showMessage(this.strings.SAVE_ACCOUNT_SUCCESS, SystemMessageStatus.Success);
    }


}
