import { Component, OnInit } from '@angular/core';
import { BasePageComponent } from 'src/app/modules/core/components/bases/base-page-component';
import { ChangePassword } from 'src/app/models/profiles/change-password.model';
import { UserService } from 'src/app/modules/core/services/user.service';
import { Router } from '@angular/router';
import { SystemMessageService } from 'src/app/modules/core/services/system-message.service';
import { SystemMessageStatus } from 'src/app/models/system-messages/system-message-types';

@Component({
    selector: 'app-change-password',
    templateUrl: './change-password.component.html',
    styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent extends BasePageComponent implements OnInit {

    public model = new ChangePassword();
    public disabled = false;
    constructor(public userService: UserService, private router: Router, private sm: SystemMessageService) {
        super(userService);
    }

    ngOnInit() {
        if (this.user) {
            this.model = new ChangePassword(this.user);
        } else {
            this.router.navigate(['/account/login']);
        }
    }

    public save() {
        if (this.model.isModelValid()) {
            this.disabled = true;
        this.userService.updateChangedPassword(this.model).then(() => {
            this.disabled = false;
            this.model.resetPasswordState();
            this.displaySuccessMessage();
        }).catch((err) => {
            this.model.isCurrentPasswordValid = false;
            this.model.resetPasswordState();
            this.disabled = false;
            throw err;
        });
    }
    }
    private displaySuccessMessage() {
        this.sm.showMessage(this.strings.SAVE_ACCOUNT_SUCCESS, SystemMessageStatus.Success);
    }
}
