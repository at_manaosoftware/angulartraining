import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseUserComponent } from '../user/components/base/base-user.component';
import { ProfileComponent } from './components/profile/profile.component';
import { RouteGuardService } from '../core/services/route-guard.service';
import { RoutePermissionModel } from 'src/app/models/permissions/router-permission.model';
import { PermissionRole } from 'src/app/models/permissions/permission-role.model';
import { PermissionAction } from 'src/app/models/permissions/permission-action.model';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { ProfileLayoutComponent } from './components/profile-layout/profile-layout.component';

const routes: Routes = [
    {
        path: '',
        component: ProfileLayoutComponent,
        children: [
            {
                path: 'view',
                component: ProfileComponent
            },
            {
                path: 'change-password',
                component: ChangePasswordComponent,
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProfileRoutingModule { }
