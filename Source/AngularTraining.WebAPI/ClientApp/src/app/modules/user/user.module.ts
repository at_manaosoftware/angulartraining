import { NgModule } from '@angular/core';
import { CoreModule } from 'src/app/modules/core/core.module';
import { UserRoutingModule } from 'src/app/modules/user/user.routes';
import { BaseUserComponent } from 'src/app/modules/user/components/base/base-user.component';
import { SidebarComponent } from 'src/app/modules/user/components/sidebars/sidebar.component';
import { CreateUserSidebarComponent } from 'src/app/modules/user/components/sidebars/views/create-user-sidebar.component';
import { UsersComponent } from 'src/app/modules/user/components/users/users.component';

@NgModule({
    declarations: [
        BaseUserComponent,
        UsersComponent,

        SidebarComponent,
        CreateUserSidebarComponent
    ],
    imports: [
        CoreModule,
        UserRoutingModule
    ],
    providers: [

    ],
    exports: [
        UserRoutingModule
    ]
})
export class UserModule { }
