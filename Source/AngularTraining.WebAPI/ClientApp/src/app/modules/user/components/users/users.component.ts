import { Component } from '@angular/core';
import { BasePageComponent } from 'src/app/modules/core/components/bases/base-page-component';
import { OnInit } from '@angular/core';
import { UserService } from 'src/app/modules/core/services/user.service';
import { UserViewModel } from 'src/app/models/users/user-view.model';
import { TableModel } from 'src/app/models/table/table.model';
import { SidebarService } from 'src/app/modules/core/services/sidebar.service';
import { SidebarModel } from 'src/app/models/sidebars/sidebar-model';
import { SidebarView } from 'src/app/models/sidebars/sidebar-view';
import { TableHelper } from 'src/app/models/helpers/table-helper';

@Component({
    selector: 'app-users-component',
    templateUrl: './users.component.html',
    styleUrls: ['./users.component.scss']
})
export class UsersComponent extends BasePageComponent implements OnInit {

    public table = new TableModel();

    constructor(public userService: UserService, private sidebar: SidebarService) {
        super(userService);
        console.log(userService);

    }

    public ngOnInit() {
        this.createTable();
    }

    public create() {
        this.sidebar.open(new SidebarModel<UserViewModel>({
            title: this.strings.CREATE_USER,
            model: new UserViewModel(),
            view: SidebarView.CREATE_USER,
            onSubmitted: () => {
                this.table.reload();
            }
        }));
    }

    private createTable() {
        this.table = TableHelper.userTable();
    }
}
