import { Injectable, Injector } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../../../environments/environment';
import { Observable, Subscription } from 'rxjs';
import { map} from 'rxjs/operators';
import { RequestSettings } from '../../../models/requests/request-settings';
import { ObjectHelper } from '../../../models/helpers/object-helper';
import { IResponse } from 'src/app/models/responses/response';
import { AuthenticationService } from 'src/app/modules/core/services/authentication.service';
import { SystemMessageService } from 'src/app/modules/core/services/system-message.service';
import { SystemMessageStatus } from 'src/app/models/system-messages/system-message-types';
import { Endpoints } from 'src/app/configs/endpoints';
import { AppTokens } from 'src/app/models/authentications/access-token.model';
import { Authentication } from 'src/app/configs/authentication';
import { Strings } from 'src/app/configs/strings';
import { UrlHelper } from 'src/app/models/helpers/url-helper';

@Injectable({
    providedIn: 'root',
})
export class RequestService {
    public static readonly INVALID_TOKEN_MESSAGE = 'The specified refresh token is invalid.';

    private baseUrl: string;
    constructor(
        private http: HttpClient,
        private auth: AuthenticationService,
        private messageService: SystemMessageService,
        private injector: Injector
    ) {
        this.baseUrl = environment.basePath;
    }

    public async get(url: string, settings?: RequestSettings): Promise<any> {

        await this.beginRequest();
        const options = this.getRequestOptions();
        return this.doRequest
        (
            this.http.get<IResponse>(this.baseUrl + url, options),
            settings
        );

    }

    public async post(url: string, data: any, settings?: RequestSettings): Promise<any> {
        await this.beginRequest();
        const options = this.getRequestOptions();
        return this.doRequest
        (
            this.http.post<IResponse>(this.baseUrl + url, ObjectHelper.toCapitalize(data), options),
            settings
        );
    }

    async formPost(url: string, data: URLSearchParams, settings?: RequestSettings): Promise<any> {
        await this.beginRequest();
        const options = this.getFormOptions();
        return this.doRequest
        (
            this.http.post<IResponse>(this.baseUrl + url, data.toString(), options),
            settings
        );
    }

    async patch(url: string, data: any, settings?: RequestSettings): Promise<any> {
        await this.beginRequest();

        const options = this.getRequestOptions();
        return this.doRequest
        (
            this.http.patch<IResponse>(this.baseUrl + url, ObjectHelper.toCapitalize(data), options),
            settings
        );

    }

    async put(url: string, data: any, settings?: RequestSettings): Promise<any> {
        await this.beginRequest();

        const options = this.getRequestOptions();
        return this.doRequest
        (
            this.http.put<IResponse>(this.baseUrl + url, ObjectHelper.toCapitalize(data), options),
            settings
        );
    }

    async delete(url: string, settings?: RequestSettings): Promise<any> {
        await this.beginRequest();
        const options = this.getRequestOptions();
        return this.doRequest
        (
            this.http.delete<IResponse>(this.baseUrl + url, options),
            settings
        );
    }

    private beginRequest() {
        return this.refreshToken();
    }

    private doRequest(req: Observable<IResponse>, settings: RequestSettings) {
        return this.requesting
        (
            req,
            settings
        );
    }

    private async requesting(req: Observable<IResponse>, setting: RequestSettings) {
        let result: IResponse;
        try {
            result = await req.toPromise();
            result = ObjectHelper.toCamel(result);
        } catch (err) {
            this.onError(err, setting);
            throw err;
        }
        return this.onSuccess(result, setting);
    }

    private async refreshToken() {
        const expires = this.auth.getTokenExpires();
        const refreshToken = this.auth.getRefreshToken();
        const date = new Date();
        if (refreshToken && expires && date >= expires) {
            const options = this.getFormOptions();
            const data = Authentication.getRefreshTokenFormData(refreshToken);
            try {
                const res = await this.http.post(this.baseUrl + Endpoints.Login, data.toString(), options)
                    .pipe(map((response) => ObjectHelper.toCamel(response)))
                    .toPromise();
                const tokens = new AppTokens(res);
                this.auth.saveToken(tokens);

            } catch (err) {
                if (err.error && err.error.error_description === RequestService.INVALID_TOKEN_MESSAGE) {
                    this.redirectToLogin();
                    throw err;
                }
            }
        }
    }

    private onSuccess(response: IResponse, setting?: RequestSettings) {
        if (setting && setting.serializer && typeof setting.serializer === 'function') {
            response = setting.serializer(response.data);
        }
        return response;
    }

    private onError(response: any, setting?: RequestSettings) {
        if (setting && setting.isCustomErrorHandle) {
            throw ObjectHelper.toCamel(response);
        } else {
            this.errorHandler(response);
        }
    }

    private errorHandler(response: any): void {
        // TODO: Handle global error response
        let errorResponseObject = response;
        if (response.statusText === 'Unknown Error') {
            this.messageService.showMessage(response.statusText, SystemMessageStatus.Danger);
        } else {
            errorResponseObject = this.extractErrorBody(response);
            if (errorResponseObject && errorResponseObject.length) {
                const message = errorResponseObject.join(',');
                this.messageService.showMessage(message, SystemMessageStatus.Danger);
            } else {
                errorResponseObject = response;
                this.messageService.showMessage(Strings.GENERAL_REQUEST_ERROR, SystemMessageStatus.Danger);
            }
        }

        throw errorResponseObject;
    }

    private extractErrorBody(res: any): Array<any> {
        if (res.error && res.error.errors) {
            return res.error.errors;
        }
        return null;
    }

    private redirectToLogin() {
        this.auth.removeToken();
        this.injector.get(Router).navigate([UrlHelper.loginUrl()]);
    }

    private getRequestOptions(): { headers: HttpHeaders } {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        const token = this.auth.getAcessToken();
        if (token) {
            httpOptions.headers = httpOptions.headers.append('Authorization', 'Bearer ' + token);
        }
        return httpOptions;
    }

    private getFormOptions(): { headers: HttpHeaders } {
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/x-www-form-urlencoded'
            })
        };
        const token = this.auth.getAcessToken();
        if (token) {
            httpOptions.headers = httpOptions.headers.append('Authorization', 'Bearer ' + token);
        }
        return httpOptions;
    }
}
