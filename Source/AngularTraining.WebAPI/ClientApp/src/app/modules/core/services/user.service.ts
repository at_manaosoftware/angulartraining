import { Injectable } from '@angular/core';
import { RequestService } from 'src/app/modules/core/services/request.service';
import { UserViewModel } from 'src/app/models/users/user-view.model';
import { Endpoints } from 'src/app/configs/endpoints';
import { AppUserModel } from 'src/app/models/users/app-user.model';
import { RequestSettings } from 'src/app/models/requests/request-settings';
import { BaseService } from 'src/app/modules/core/services/base.service';
import { IService } from 'src/app/modules/core/services/interface.service';
import { ServiceEndpoints } from 'src/app/models/endpoints/service-endpoints.model';
import { ForgetPasswordModel } from 'src/app/models/accounts/forget-password.model';
import { RecoveryPasswordModel } from '../../../models/accounts/recovery-password.model';
import { AccountModel } from '../../../models/accounts/account-model';
import { Subject } from 'rxjs';
import { Profile } from 'src/app/models/profiles/profile.model';
import { ChangePassword } from 'src/app/models/profiles/change-password.model';

@Injectable({
    providedIn: 'root'
})
export class UserService extends BaseService<UserViewModel> implements IService<UserViewModel> {

    private user: AppUserModel;

    private onLoginSubject = new Subject<void>();

    constructor(public request: RequestService) {
        super(request, new ServiceEndpoints({
            get: '',
            getAll: Endpoints.GetAllUsers,
            create: Endpoints.CreateUser,
            update: Endpoints.UpdateUser,
            delete: '',
        }), UserViewModel.makeList);

    }

    public updateUser(model: AccountModel): Promise<any> {
        return this.request.patch(this.endpoints.update, model, new RequestSettings({isCustomErrorHandle: true}));
    }
    public updateUserProfile(model: Profile): Promise<any> {
        return this.request.patch(this.endpoints.update, model, new RequestSettings({isCustomErrorHandle: true}));
    }
    public updateChangedPassword(model: ChangePassword): Promise<any> {
        return this.request.patch(this.endpoints.update, model, new RequestSettings({isCustomErrorHandle: true}));
    }

    async getCurrentUserAsync(): Promise<AppUserModel> {
        const res = await this.request.get(Endpoints.GetCurrentUser,
            new RequestSettings({isCustomErrorHandle: true}
            ));
        this.user = new AppUserModel(res);
        this.broadcastOnLogin();
        return this.user;
    }

    async getCurrentUserAsyncIgnore401() {
        try {
            await this.getCurrentUserAsync();
        } catch (e) {
            if (e.status === 401) {
                // swallow 401 errors
                return;
            }
            throw e;
        }
    }


    public recoveryPassword(model: ForgetPasswordModel) {
        return this.request.post(Endpoints.RecoveryPassword, model);
    }

    public resetPassword(model: RecoveryPasswordModel) {
        return this.request.post(Endpoints.ResetPassword, model);
    }

    public getCurrentUserSync(): AppUserModel {
        return this.user;
    }

    public removeUser() {
        this.user = null;
    }

    public subscribeUserLogin(callback: () => void) {
        return this.onLoginSubject.subscribe(value => callback());
    }

    private broadcastOnLogin() {
        this.onLoginSubject.next();
    }
}
