import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/modules/core/services/user.service';
import { PermissionHelper } from 'src/app/models/helpers/permission-helper';
import { concat } from 'rxjs/internal/operators/concat';

@Injectable({
    providedIn: 'root',
})
export class RouteGuardService implements CanActivate {

    constructor(private router: Router, private userService: UserService) {

    }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const routePermissions = route.data.permissions;
        if (!routePermissions || routePermissions.length === 0) {
            return true;
        }
        const user = this.userService.getCurrentUserSync();
        if (user) {
            const hasPermission = PermissionHelper.hasPermission(user.permissions, routePermissions);

            if (!hasPermission) {
                this.userService.removeUser();
                this.router.navigate(['/account/login']);
            }

            return hasPermission;
        }
        this.userService.removeUser();
        this.router.navigate(['/account/login']);
        return false;
    }
}
