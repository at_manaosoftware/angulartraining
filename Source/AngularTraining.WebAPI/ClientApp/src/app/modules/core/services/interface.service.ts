import { RequestService } from 'src/app/modules/core/services/request.service';
import { Page } from 'src/app/models/paging/page.model';

export interface IService<T> {
    request: RequestService;
    getAll(page: number): Promise<Page<Array<T>>>;
    get(id: number): Promise<T>;
    create(model: T): Promise<any>;
    update(model: T): Promise<any>;
    delete(id: number): Promise<any>;
}
