import {SystemMessageService} from './system-message.service';
import {fakeAsync, TestBed, tick} from '@angular/core/testing';
import {RequestService} from './request.service';
import {HttpClientTestingModule, HttpTestingController, TestRequest} from '@angular/common/http/testing';
import {environment} from '../../../../environments/environment';
import SpyObj = jasmine.SpyObj;
import {AuthenticationService} from './authentication.service';
import {Endpoints} from '../../../configs/endpoints';
import {AppTokens} from '../../../models/authentications/access-token.model';
import {Router} from '@angular/router';
import {RequestSettings} from '../../../models/requests/request-settings';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

describe('requestService', () => {
    const rejectSpy = jasmine.createSpy('onRejected');
    let messageServiceSpy: SpyObj<SystemMessageService>;
    let authServiceSpy: SpyObj<AuthenticationService>;
    let routerSpy: SpyObj<Router>;
    let requestService: RequestService;
    let httpController: HttpTestingController;
    const accessToken = 'myAccessToken';

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
            providers: [
                RequestService,
                {
                    provide: SystemMessageService,
                    useValue: jasmine.createSpyObj('systemMessageService', ['showMessage'])
                },
                {
                    provide: AuthenticationService,
                    useValue: jasmine.createSpyObj('authService', [
                        'getTokenExpires',
                        'getRefreshToken',
                        'getAcessToken',
                        'saveToken',
                        'removeToken'
                    ])
                },
                {
                    provide: Router,
                    useValue: jasmine.createSpyObj('router', [
                        'navigate'
                    ])
                }
            ]
        });

        messageServiceSpy = TestBed.get(SystemMessageService);
        routerSpy = TestBed.get(Router);

        requestService = TestBed.get(RequestService);

        httpController = TestBed.get(HttpTestingController);

        authServiceSpy = TestBed.get(AuthenticationService);
        authServiceSpy.getAcessToken.and.returnValue(accessToken);

    });

    afterEach(() => {
        httpController.verify();
    });

    it('should prepend the url with the base url from environment', fakeAsync(() => {
        requestService.get('hello');
        tick();

        const testRequest = httpController.expectOne(environment.basePath + 'hello');
        expect(testRequest.request.method).toEqual('GET');
        testRequest.flush({});
    }));

    describe('access token', () => {
        it('should be in the auth header', fakeAsync(() => {
            requestService.get('hello');
            tick();

            const testRequest = httpController.expectOne(environment.basePath + 'hello');
            const headers = testRequest.request.headers;
            expect(headers.has('Authorization')).toBeTruthy();
            expect(headers.get('Authorization')).toContain(accessToken);
            testRequest.flush({});
        }));

        it(`should not be passed there isn't one`, fakeAsync(() => {
            authServiceSpy.getAcessToken.and.returnValue(null);

            requestService.get('hello');
            tick();

            const testRequest = httpController.expectOne(environment.basePath + 'hello');
            const headers = testRequest.request.headers;
            expect(headers.has('Authorization')).toBeFalsy();
            testRequest.flush({});
        }));
    });
    describe('refresh', () => {

        function dateOffsetBySeconds(seconds: number) {
            return new Date(new Date().valueOf() + (1000 * 60 * seconds));
        }

        const expectedRefreshToken = 'refreshWithMe';
        // date is one minute from now by default
        const refreshExpiresDate: Date = dateOffsetBySeconds(60);
        beforeEach(() => {
            authServiceSpy.getRefreshToken.and.returnValue(expectedRefreshToken);
            authServiceSpy.getTokenExpires.and.returnValue(refreshExpiresDate);
        });
        it(`should not be called when the refresh token has not expired`, fakeAsync(() => {
            requestService.get('hello');
            tick();

            httpController.expectOne(environment.basePath + 'hello').flush({});
        }));
        it(`should be called when the refresh token has expired`, () => {
            authServiceSpy.getTokenExpires.and.returnValue(dateOffsetBySeconds(-10));

            requestService.get('hello');

            const loginRequest = httpController.expectOne(environment.basePath + Endpoints.Login);
            expect(loginRequest.request.method).toBe('POST');
            expect(loginRequest.request.body).toContain(expectedRefreshToken);
        });

        it(`should only call the original request after it's done`, fakeAsync(() => {
            authServiceSpy.getTokenExpires.and.returnValue(dateOffsetBySeconds(-10));

            requestService.get('hello');

            const loginRequest = httpController.expectOne(environment.basePath + Endpoints.Login);

            httpController.expectNone(environment.basePath + 'hello');
            loginRequest.flush({});
            tick();
            httpController.expectOne(environment.basePath + 'hello');
        }));

        it(`should redirect to login on bad refresh token`, fakeAsync(() => {
            authServiceSpy.getTokenExpires.and.returnValue(dateOffsetBySeconds(-10));
            requestService.get('hello').catch(() => {
            });
            tick();

            const loginRequest = httpController.expectOne(environment.basePath + Endpoints.Login);
            const error = {error_description: RequestService.INVALID_TOKEN_MESSAGE};
            loginRequest.flush(error, {status: 500, statusText: 'Server error, invalid token'});
            tick();
            expect(authServiceSpy.removeToken).toHaveBeenCalled();
            expect(routerSpy.navigate).toHaveBeenCalled();
        }));

        it(`should continue if there's a different error`, fakeAsync(() => {
            authServiceSpy.getTokenExpires.and.returnValue(dateOffsetBySeconds(-10));
            requestService.get('hello').catch(() => {
            });
            tick();

            const loginRequest = httpController.expectOne(environment.basePath + Endpoints.Login);

            const error = {error_description: 'some other error message'};
            loginRequest.flush(error, {status: 500, statusText: 'Server error, invalid token'});
            tick();

            httpController.expectOne(environment.basePath + 'hello');
        }));

        it(`should use the response to set the new auth token`, fakeAsync(() => {
            authServiceSpy.getTokenExpires.and.returnValue(dateOffsetBySeconds(-10));
            requestService.get('hello').catch(() => {
            });
            tick();

            const loginRequest = httpController.expectOne(environment.basePath + Endpoints.Login);
            const newTokenValue = 'myNewToken';
            loginRequest.flush({[AppTokens.ACCESS_TOKEN_KEY]: newTokenValue});
            tick();

            const newAppToken = <AppTokens>authServiceSpy.saveToken.calls.mostRecent().args[0];
            expect(newAppToken.accessToken).toBe(newTokenValue);

            httpController.expectOne(environment.basePath + 'hello');
        }));
    });

    describe('errors', () => {
        let testRequest: TestRequest;

        function setup(settings?: any) {
            requestService.get('hello', new RequestSettings(settings)).then(fail, (e) => {
                rejectSpy(e);
            });
            tick();

            testRequest = httpController.expectOne(environment.basePath + 'hello');
        }

        it('should send back an error response', fakeAsync(() => {
            setup();
            const errorResponse = {myError: 'oops'};
            testRequest.flush(errorResponse, {status: 500, statusText: 'server error'});
            tick(environment.lazyTimeMs + 5);

            expect(rejectSpy).toHaveBeenCalled();
            const callInfo = rejectSpy.calls.mostRecent();
            expect(callInfo.args[0].error).toBe(errorResponse);
        }));

        it(`should show errors to the user via the message service`, fakeAsync(() => {
            setup();
            testRequest.flush({}, {status: 500, statusText: 'server error'});
            tick();

            expect(messageServiceSpy.showMessage).toHaveBeenCalled();
        }));

        it('should use response errors for message', fakeAsync(() => {
            setup();
            const expectedError = 'This is my error';
            testRequest.flush({errors: [expectedError]}, {status: 500, statusText: 'server error'});
            tick();

            expect(messageServiceSpy.showMessage).toHaveBeenCalled();
            const callInfo = messageServiceSpy.showMessage.calls.mostRecent();
            expect(callInfo.args[0]).toContain(expectedError);
        }));

        it(`should show unknown error when that's the status`, fakeAsync(() => {
            setup();
            const expectedError = 'Unknown Error';

            testRequest.flush({errors: ['This is my error']}, {status: 500, statusText: expectedError});
            tick();

            expect(messageServiceSpy.showMessage).toHaveBeenCalled();
            const callInfo = messageServiceSpy.showMessage.calls.mostRecent();
            expect(callInfo.args[0]).toContain(expectedError);
        }));

        it('should send back the response for a custom error handler', fakeAsync(() => {
            setup({isCustomErrorHandle: true});
            const errorResponse = {myError: 'oops'};
            testRequest.flush(errorResponse, {status: 500, statusText: 'server error'});
            tick(environment.lazyTimeMs + 5);
            expect(rejectSpy).toHaveBeenCalled();
            const response: HttpErrorResponse = rejectSpy.calls.mostRecent().args[0];
            expect(response.url).toBe('/api/v1/hello');
            expect(response.error).toEqual(errorResponse);
        }));
    });

    describe('response', () => {
        it('should get converted to camel case', fakeAsync(() => {
            let response: any = {};
            requestService.get('hello').then(r => response = r, fail);
            tick();

            const testRequest = httpController.expectOne(environment.basePath + 'hello');
            testRequest.flush({Hello: 'world'});
            tick(environment.lazyTimeMs + 500);
            expect(Object.keys(response)).toContain('hello');
        }));

        it('should use the response from custom serializer setting', fakeAsync(() => {
            let actualResponse = {};
            const serializerResponse = {};
            requestService.get('hello', new RequestSettings({serializer: () => serializerResponse}))
                .then(r => actualResponse = r, fail);
            tick();

            const testRequest = httpController.expectOne(environment.basePath + 'hello');
            const requestResponse = {};
            testRequest.flush(requestResponse);
            tick(environment.lazyTimeMs + 500);

            expect(actualResponse).not.toBe(requestResponse);
            expect(actualResponse).toBe(serializerResponse);
        }));
    });
});
