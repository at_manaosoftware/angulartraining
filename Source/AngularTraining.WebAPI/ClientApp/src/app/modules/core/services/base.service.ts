import {IService} from 'src/app/modules/core/services/interface.service';
import {RequestService} from 'src/app/modules/core/services/request.service';
import {ServiceEndpoints} from 'src/app/models/endpoints/service-endpoints.model';
import {Page} from 'src/app/models/paging/page.model';
import {PagingSerializer} from 'src/app/models/serializers/paging-serializer';
import {RequestSettings} from 'src/app/models/requests/request-settings';

export class BaseService<T> implements IService<T> {

    public request: RequestService;
    public endpoints: ServiceEndpoints;
    public dataSerializer:  (data: any) => T | T[];

    constructor(
        public rq: RequestService,
        public serviceEndpoints?: ServiceEndpoints,
        public serializer?:  (data: any) => T | T[]
    ) {
        this.endpoints = serviceEndpoints;
        this.request = rq;
        this.dataSerializer = serializer;
    }

    public get(id: number): Promise<T> {
        return this.request.get(this.endpoints.get + '/' + id);
    }

    public async getAll(page: number): Promise<Page<T[]>> {
        const res = await this.request.get(this.endpoints.getAll + '?page=' + page, new RequestSettings({
            serializer: PagingSerializer.serialize
        }));
        if (this.dataSerializer) {
            res.data = this.dataSerializer(res.data);
        }
        return res;
    }

    public create(model: T): Promise<any> {
        return this.request.post(this.endpoints.create, model);
    }

    public update(model: T): Promise<any> {
        return this.request.patch(this.endpoints.update, model);
    }

    public delete(id: number): Promise<any> {
        return this.request.delete(this.endpoints.delete + '/' + id);
    }
}
