import { OnDestroy, OnInit } from '@angular/core';
import { Strings } from 'src/app/configs/strings';
import { AppUserModel } from '../../../../models/users/app-user.model';
import { UserService } from '../../services/user.service';

export class BaseSidebarViewComponent implements OnInit {

    public strings = Strings;

    public user: AppUserModel;

    constructor(public userService: UserService) {
        this.user = userService.getCurrentUserSync();
    }

    public ngOnInit() {

    }
}
