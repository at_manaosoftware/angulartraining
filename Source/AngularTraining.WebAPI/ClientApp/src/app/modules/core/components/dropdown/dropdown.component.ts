import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IResponse } from '../../../../models/responses/response';
import { StringHelper } from '../../../../models/helpers/string-helper';
import { RequestService } from '../../services/request.service';
import { Option } from '../../../../models/options/option.model';

@Component({
    selector: 'app-dropdown-component',
    templateUrl: './dropdown.component.html',
    styleUrls: ['./dropdown.component.scss']
})

export class DropdownComponent implements OnInit {

    public value: string;

    @Input() options: Array<Option> = [];

    @Input() url: string;

    @Input()
    get model() {
        return this.value;
    }
    set model(val) {
        this.value = val;
        this.modelChange.emit(this.value);
    }

    @Output() modelChange = new EventEmitter<string>();

    public id = StringHelper.randomString();

    constructor(private request: RequestService) {

    }

    public ngOnInit() {
        this.initOptions();
    }

    public onChange() {
        this.model = this.value;
        console.log(this.value);
    }

    private initOptions() {
        if (this.url) {
            this.loadOptions();
        }
    }

    private loadOptions() {
        this.request.get(this.url).then((res: IResponse) => {
            this.options = res.data;
        }).catch((err) => {
            this.options = [];
            throw err;
        });
    }
}
