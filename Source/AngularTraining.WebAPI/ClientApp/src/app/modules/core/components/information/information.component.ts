import { Component, OnInit } from '@angular/core';
import { TableModel } from 'src/app/models/table/table.model';
import { Strings } from 'src/app/configs/strings';
import { TableHelper } from 'src/app/models/helpers/table-helper';

@Component({
    selector: 'app-information',
    templateUrl: './information.component.html',
    styleUrls: ['./information.component.css']
})
export class InformationComponent implements OnInit {

    public title = 'Welcome to my web application';
    public versions = [1, 2, 3, 4, 5];
    public table = new TableModel();
    public strings = Strings;
    constructor() { }

    ngOnInit() {
        this.getSchedule();
    }
    test(e: any) {
        console.log(e);

    }
    public getSchedule() {
        this.table = TableHelper.scheduleTable();
    }

}
