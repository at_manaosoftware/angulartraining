import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-information-view',
    templateUrl: './information-view.component.html',
    styleUrls: ['./information-view.component.css']
})
export class InformationViewComponent implements OnInit {

    public id = '123456';
    constructor(private routera: Router, private activeRoute: ActivatedRoute
    ) { }

    ngOnInit() {

    }
    public navigateByUrl() {
        this.routera.navigateByUrl('/a');
    }
    public navigate(value: string) {
        this.routera.navigate([value], { queryParams: { id: this.id }, relativeTo: this.activeRoute });
    }
}
