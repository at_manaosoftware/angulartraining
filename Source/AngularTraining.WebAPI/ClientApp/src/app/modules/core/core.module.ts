import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HomeComponent } from 'src/app/modules/core/components/home/home.component';
import { PageNotFoundComponent } from 'src/app/modules/core/components/page-not-found/page-not-found.component';
import { PaggingComponent } from 'src/app/modules/core/components/pagging/pagging.component';
import { NavigationComponent } from 'src/app/modules/core/components/navigation/navigation.component';
import { FooterComponent } from 'src/app/modules/core/components/footer/footer.component';
import { SystemMessageComponent } from 'src/app/modules/core/components/system-message/system-message.component';
import { TableComponent } from 'src/app/modules/core/components/table/table.component';
import { SpinnerComponent } from 'src/app/modules/core/components/spinner/spinner.component';
import { DropdownComponent } from './components/dropdown/dropdown.component';
import { CollapseModule } from 'ngx-bootstrap';
import { ContentListComponent } from './components/content-list/content-list.component';
import { DashBoardComponent } from './components/dash-board/dash-board.component';
import { InformationComponent } from './components/information/information.component';
import { InformationViewComponent } from './components/information-view/information-view.component';

@NgModule({
    declarations: [
        HomeComponent,
        PageNotFoundComponent,
        PaggingComponent,
        NavigationComponent,
        FooterComponent,
        SystemMessageComponent,
        TableComponent,
        SpinnerComponent,
        DropdownComponent,
        ContentListComponent,
        DashBoardComponent,
        InformationComponent,
        InformationViewComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        RouterModule,
        CollapseModule
    ],
    providers: [

    ],
    exports: [
        CommonModule,
        FormsModule,
        HomeComponent,
        PageNotFoundComponent,
        PaggingComponent,
        NavigationComponent,
        FooterComponent,
        SystemMessageComponent,
        TableComponent,
        SpinnerComponent,
        DropdownComponent,
        CollapseModule
    ]
})
export class CoreModule { }
