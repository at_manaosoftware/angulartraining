import { Component } from '@angular/core';
import { UserService } from 'src/app/modules/core/services/user.service';
import { BaseSidebarComponent } from 'src/app/modules/core/components/bases/base-sidebar-component';
import { SidebarService } from 'src/app/modules/core/services/sidebar.service';
import { SidebarModel } from 'src/app/models/sidebars/sidebar-model';

@Component({
    selector: 'app-sidebar-component',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent extends BaseSidebarComponent {

    constructor(public userService: UserService, public sidebarService: SidebarService) {
        super(userService, sidebarService);
    }
}
