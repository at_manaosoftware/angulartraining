import { Component } from '@angular/core';
import { LoginModel } from 'src/app/models/accounts/login.model';
import { BasePageComponent } from 'src/app/modules/core/components/bases/base-page-component';
import { RequestService } from 'src/app/modules/core/services/request.service';
import { Endpoints } from 'src/app/configs/endpoints';
import { AuthenticationService } from 'src/app/modules/core/services/authentication.service';
import { AppTokens } from 'src/app/models/authentications/access-token.model';
import { Router } from '@angular/router';
import { UserService } from 'src/app/modules/core/services/user.service';

@Component({
    selector: 'app-login-component',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BasePageComponent {

    public model = new LoginModel();

    public disabled = false;

    public isLoad = false;

    constructor(
        public userService: UserService,
        private request: RequestService,
        private auth: AuthenticationService,
        private router: Router
    ) {
        super(userService);
    }

    public onLogin() {
        if (this.model.isModelValid()) {
            this.loading();
            this.request.formPost(Endpoints.Login, this.model.toFormData()).then((res) => {
                const tokens = new AppTokens(res);
                this.auth.saveToken(tokens);
                return this.getUserInfo();
            }).catch((err) => {
                this.onError();
                throw err;
            });
        }
    }

    public onKeyPress(event) {
        if (event.keyCode === 13) {
            this.onLogin();
        }
    }

    private getUserInfo() {
        return this.userService.getCurrentUserAsync().then((u) => {
            return this.router.navigate(['/']);
        }).catch((err) => {
            this.onError();
            throw err;
        });
    }

    private loading() {
        this.model.isFail = false;
        this.isLoad = true;
        this.disableForm();
    }

    private onError() {
        this.model.isFail = true;
        this.enableFrom();
    }

    private disableForm() {
        this.disabled = true;
    }

    private enableFrom() {
        this.isLoad = false;
        this.disabled = false;
    }
}
