import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from 'src/app/modules/accounts/components/login/login.component';
import { ForgetPasswordComponent } from 'src/app/modules/accounts/components/forget-password/forget-password.component';
import { RecoveryPasswordComponent } from './components/recovery-password/recovery-password.component';
import { AccountComponent } from './components/account/account.component';
import { HomeComponent } from '../core/components/home/home.component';

const accountRoutes: Routes = [
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: 'test',
        component: AccountComponent,
    },
    {
        path: '',
        children: [
            {
                path: '',
                component: AccountComponent,
            },
            {
                path: 'login',
                component: LoginComponent,
            },
            {
                path: 'forget-password',
                component: ForgetPasswordComponent,
            },
            {
                path: 'recovery-password',
                component: RecoveryPasswordComponent,
            }
        ]
    },
];
@NgModule({
    imports: [
        RouterModule.forChild(accountRoutes)
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule { }
