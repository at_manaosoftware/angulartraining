import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from 'src/app/modules/core/components/page-not-found/page-not-found.component';
import { HomeComponent } from 'src/app/modules/core/components/home/home.component';
import { PreloadAllModules } from '@angular/router';
import { RouteGuardService } from 'src/app/modules/core/services/route-guard.service';
import { RoutePermissionModel } from 'src/app/models/permissions/router-permission.model';
import { PermissionRole } from 'src/app/models/permissions/permission-role.model';
import { PermissionAction } from 'src/app/models/permissions/permission-action.model';
import { ContentListComponent } from './modules/core/components/content-list/content-list.component';
import { DashBoardComponent } from './modules/core/components/dash-board/dash-board.component';
import { InformationComponent } from './modules/core/components/information/information.component';
import { InformationViewComponent } from './modules/core/components/information-view/information-view.component';

const appRoutes: Routes = [
    // {
    //     path: '',
    //     component: HomeComponent,
    //     canActivate: [
    //         RouteGuardService
    //     ],
    //     data: {
    //         permissions: [
    //             [new RoutePermissionModel({ permission: PermissionRole.Dashboard, actions: [PermissionAction.Read]})]
    //         ]
    //     }
    // },
    {
        path: '',
        component: HomeComponent,
        children: [
            {
                path: '',
                component: DashBoardComponent,
                canActivate: [
                    RouteGuardService
                ],
                data: {
                    permissions: [
                        [new RoutePermissionModel({ permission: PermissionRole.Users, actions: [PermissionAction.Read] })]
                    ]
                }
            },
            {
                path: 'information',
                children: [
                    {
                        path: '',
                        component: InformationComponent
                    },
                    {
                        path: 'view/test',
                        component: InformationViewComponent
                    }
                ]
            },
            {
                path: 'users',
                loadChildren: './modules/user/user.module#UserModule'
            },
            {
                path: 'tenants',
                loadChildren: './modules/tenant/tenant.module#TenantModule'
            },
            {
                path: 'profiles',
                loadChildren: './modules/profile/profile.module#ProfileModule'
            }
        ]
    },
    {
        path: 'account',
        loadChildren: './modules/accounts/account.module#AccountModule'
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            appRoutes,
            { preloadingStrategy: PreloadAllModules, enableTracing: false }
        )
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule { }
