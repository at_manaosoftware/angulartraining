import { TableModel } from 'src/app/models/table/table.model';
import { Endpoints } from 'src/app/configs/endpoints';
import { TableColumnModel } from 'src/app/models/table/table-column.model';
import { TableColumnTypes } from 'src/app/models/table/table-column-types';
import { Strings } from 'src/app/configs/strings';

export class TableHelper {

    static userTable(): TableModel {
        const columns = [
            new TableColumnModel({
                title: Strings.USER_NAME,
                key: 'username',
                dataType: TableColumnTypes.Text,
                width: 30
            }),
            new TableColumnModel({
                title: Strings.FIRST_NAME,
                key: 'firstName',
                dataType: TableColumnTypes.Text,
                width: 30
            }),
            new TableColumnModel({
                title: Strings.LAST_NAME,
                key: 'lastName',
                dataType: TableColumnTypes.Text,
                width: 30
            })
        ];
        return new TableModel({
            columns: columns,
            sourceUrl: Endpoints.GetAllUsers
        });
    }

    static tenantTable(): TableModel {
        const columns = [
            new TableColumnModel({
                title: Strings.ID,
                key: 'id',
                dataType: TableColumnTypes.Text,
                width: 30
            }),
            new TableColumnModel({
                title: Strings.NAME,
                key: 'name',
                dataType: TableColumnTypes.Text,
                width: 30
            }),
            new TableColumnModel({
                title: Strings.USERS,
                key: 'usersCount',
                dataType: TableColumnTypes.Text,
                width: 30
            })
        ];

        return new TableModel({
            columns: columns,
            sourceUrl: Endpoints.GetAllTenants
        });
    }
    static scheduleTable(): TableModel {
        const columns = [
            new TableColumnModel({
                title: Strings.SUBJECT,
                key: 'subject',
                dataType: TableColumnTypes.Text,
                width: 30
            }),
            new TableColumnModel({
                title: Strings.ROOM,
                key: 'roomNumber',
                dataType: TableColumnTypes.Text,
                width: 30
            }),
            new TableColumnModel({
                title: Strings.DATE,
                key: 'startTime',
                dataType: TableColumnTypes.Date,
                width: 30
            }),
            new TableColumnModel({
                title: Strings.TIME,
                key: 'startTime',
                dataType: TableColumnTypes.DateTime,
                width: 30
            })
        ];

        return new TableModel({
            columns: columns,
            sourceUrl: Endpoints.GetAllSchedules
        });
    }
}
