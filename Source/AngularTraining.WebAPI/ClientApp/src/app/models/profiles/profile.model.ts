export class Profile {
    public id: string;
    public name: string;
    public email: string;
    public firstName: string;
    public lastName: string;
    public isFirstNameValid = true;

    constructor(data?: any) {
        if (data) {
            this.cast(data);
        }
    }

    private cast(data) {
        this.id = data.id;
        this.name = data.name;
        this.email = data.email;
        this.firstName = data.firstName;
        this.lastName = data.lastName;
    }
    public isModelValid(): boolean {
        let isValid = true;
        this.resetModelState();
        if (!this.firstName.trim()) {
            this.isFirstNameValid = false;
            isValid = false;
        }
        return isValid;
    }
    private resetModelState() {
        this.isFirstNameValid = true;
    }

}
