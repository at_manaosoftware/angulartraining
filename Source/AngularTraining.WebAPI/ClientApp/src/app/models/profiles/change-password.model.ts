import { ValidationHelper } from '../helpers/validation-helper';

export class ChangePassword {
    public id: string;
    public password: string;
    public newPassword: string;
    public confirmPassword: string;
    public isFirstNameValid = true;
    public isNewPasswordValid = true;
    public isConfirmPasswordValid = true;
    public isCurrentPasswordValid = true;
    public isCurrentPasswordRequiredValid = true;

    constructor(data?: any) {
        if (data) {
            this.cast(data);
        }
    }
    private cast(data) {
        this.id = data.id;
        this.password = '';
        this.confirmPassword = '';
        this.newPassword = '';
    }
    public isModelValid(): boolean {
        let isValid = true;
        this.resetModelState();

        if (this.newPassword) {
            if (this.newPassword !== this.confirmPassword) {
                this.isConfirmPasswordValid = false;
                isValid = false;
            }
            if (!ValidationHelper.isValidPassword(this.newPassword)) {
                this.isNewPasswordValid = false;
                isValid = false;
            }
            if (!this.password) {
                this.isCurrentPasswordRequiredValid = false;
                isValid = false;
            }
        }

        return isValid;
    }
    public resetPasswordState() {
        this.password = '';
        this.newPassword = '';
        this.confirmPassword = '';
    }

    private resetModelState() {
        this.isNewPasswordValid = true;
        this.isConfirmPasswordValid = true;
        this.isCurrentPasswordValid = true;
        this.isCurrentPasswordRequiredValid = true;
    }
}
