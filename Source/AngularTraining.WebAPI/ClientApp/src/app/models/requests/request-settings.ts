export class RequestSettings {

    public withCredentials: boolean;

    public serializer: (data: any) => any;

    public isCustomErrorHandle: boolean;

    constructor(data?: any) {
        if (data) {
            this.cast(data);
        }
    }

    private cast(data) {
        this.withCredentials = !!data.withCredentials;
        this.isCustomErrorHandle = !!data.isCustomErrorHandle;
        this.serializer = data.serializer;
    }
}
