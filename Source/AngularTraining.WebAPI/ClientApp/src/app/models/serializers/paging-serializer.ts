import { PagingResponse } from 'src/app/models/responses/paging-response';

interface IPagingModel {
    hasNextPage: boolean;
    hasPreviousPage: boolean;
    list: Array<any>;
    pageNumber: number;
    pageSize: number;
    previousPageNumber: number;
    totalItems: number;
    totalPages: number;
}

export class PagingSerializer {
    static serialize(data: IPagingModel): PagingResponse {
        return new PagingResponse({
            totalItems: data.totalItems,
            data: data.list,
            pageSize: data.pageSize
        });
    }
}
