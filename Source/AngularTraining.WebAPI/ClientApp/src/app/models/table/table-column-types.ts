export const enum TableColumnTypes {
    Text = 'text',
    Date = 'date',
    DateTime = 'datetime'
}
