import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CoreModule } from 'src/app/modules/core/core.module';
import { AccountModule } from 'src/app/modules/accounts/account.module';
import { AppRoutingModule } from 'src/app/app.routes';
import { AppComponent } from './app.component';
import { UserService } from 'src/app/modules/core/services/user.service';

// export function doAuthentication(userService: UserService) {
//     return () => userService.getCurrentUserAsyncIgnore401();
// }

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        CoreModule,
        AppRoutingModule,
    ],
    providers: [
        // {
        //     provide: APP_INITIALIZER,
        //     useFactory: doAuthentication,
        //     deps: [UserService],
        //     multi: true
        // }
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule { }
