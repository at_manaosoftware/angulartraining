using AngularTraining.Core.CQS.ClassSchedules.Queries;
using AngularTraining.Core.DomainModels.ClassSchedules;
using AngularTraining.Core.Helpers.Paginations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTraining.WebAPI.Controllers.V1
{
    [Produces("application/json")]
    [Route("api/v1/classSchedule")]
    public class ClassScheduleController : BaseController
    {
        public ClassScheduleController(IContainer container) : base(container)
        {
        }
        [HttpGet("queryAll")]
        [AllowAnonymous]
        public IActionResult GetAllSchedule(GetAllSchedulesQuery allscheduleQuery)
        {
            return APIResponse(WebApiQueryParser<GetAllSchedulesQuery, PagedList<ClassScheduleDTO>>(allscheduleQuery));
        }
    }
}