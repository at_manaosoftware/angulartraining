using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StructureMap;

namespace AngularTraining.WebAPI.Controllers.V1
{
    [Route("api/v1/version")]
    public class VersionController : BaseController
    {
        public VersionController(IContainer container) : base(container)
        {
        }

        [HttpGet]
        [AllowAnonymous]
        [Produces("text/plain")]
        public IActionResult Version()
        {
            return Content(typeof(Startup).Assembly.GetName().Version.ToString());
        }
    }
}