using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.DomainModels.UserRoles;
using AngularTraining.Core.Externals.Dispatcher;
using AngularTraining.Core.Externals.Identities;
using AngularTraining.Core.Externals.Repositories;
using AngularTraining.Core.Helpers.UOWExtensions;
using AngularTraining.WebAPI.Helpers;
using AngularTraining.WebAPI.Models.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StructureMap;

namespace AngularTraining.WebAPI.Controllers.V1
{
    public class BaseController : Controller
    {
        protected readonly IContainer container;
        protected readonly ICommandBus commandBus;
        protected readonly IQueryParser parser;

        public BaseController(IContainer container)
        {
            this.container = container;
            this.commandBus = container.GetInstance<ICommandBus>();
            this.parser = container.GetInstance<IQueryParser>();
        }

        protected IActionResult APIResponse(object result, int failureStatusCode = StatusCodes.Status500InternalServerError)
        {
            var apiResponse = new APIResponseWrapper();
            if (result is ValidationResult)
            {
                apiResponse.Errors = (result as ValidationResult).Errors;
                HttpContext.Response.StatusCode = failureStatusCode;

            }
            else if(result is CommandResult)
            {
                apiResponse.Errors = (result as CommandResult).Errors;
                HttpContext.Response.StatusCode = (result as CommandResult).Success ? StatusCodes.Status200OK : failureStatusCode;
            }
            else
            {
                apiResponse.Data = result;
                HttpContext.Response.StatusCode = StatusCodes.Status200OK;
            }
            
            return new ObjectResult(apiResponse);
        }

        protected async Task<ICommandResult> WebApiCommandDispatcherAsync<T>(T command) where T : ICommand
        {
            ValidationResult validationResult = commandBus.Validate(command);
            if (validationResult.IsValid)
            {
                var result = await commandBus.SubmitAsync(command);
                return result;
            }
            else
                 return validationResult;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<UserViewModel> GetCurrentUser()
        {
            var claimsIdentity = HttpContext.User.Identity as ClaimsIdentity;
            if (HttpContext.User.Identity.IsAuthenticated && claimsIdentity != null)
            {
                var appUserId = claimsIdentity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;

                var applicationUser = await container.GetInstance<IIdentityUserManager>().FindByIdAsync(appUserId);
                if (applicationUser == null)
                    return null;

                var appUser = new UserViewModel()
                {
                    Id = applicationUser.Id,
                    Email = applicationUser.Email,
                    Name = applicationUser.FirstName + " " + applicationUser.LastName,
                    FirstName = applicationUser.FirstName,
                    LastName = applicationUser.LastName,
                };

                if (!applicationUser.IsSuperAdministrator)
                {
                    var user = await container.GetInstance<IUnitOfWork>().UsersDBSet.GetUserByAppIdIncludeAppUser(appUserId);
                    if (user == null)
                        return null;

                    appUser.TenantId = user.TenantId;
                    appUser.Permissions = user.UserRoleObject;
                }
                else
                {
                    appUser.Permissions = new SuperAdministrator();
                }

                return appUser;
            }

            return null;
        }
        
        protected R WebApiQueryParser<Q, R>(Q query) where Q : IQuery<R>
        {
            R result = parser.Process(query);
            return result;
        }
    }
}