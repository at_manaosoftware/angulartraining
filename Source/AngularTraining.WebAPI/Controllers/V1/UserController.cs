using System.Threading.Tasks;
using AngularTraining.Core.CQS.Users;
using AngularTraining.Core.CQS.Users.Commands;
using AngularTraining.Core.CQS.Users.Queries;
using AngularTraining.Core.DomainModels.Users;
using AngularTraining.Core.Helpers.Paginations;
using AngularTraining.WebAPI.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StructureMap;
using static AngularTraining.WebAPI.Authentication.RolePermissionFilter;

namespace AngularTraining.WebAPI.Controllers.V1
{
    [Produces("application/json")]
    [Route("api/v1/user")]
    public class UserController : BaseController
    {
        public UserController(IContainer container) : base(container)
        {

        }

        [HttpGet("me")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "API")]
        [AuthorizePermission(Permission.CanReadUsers)]
        public async Task<IActionResult> CurrentUser()
        {
            return Ok(await GetCurrentUser());
        }

        [HttpGet("query/users")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "API")]
        [AuthorizePermission(Permission.CanReadUsers)]
        public IActionResult GetAllUsers(GetAllUsersQuery alluserQuery)
         {
            return APIResponse(WebApiQueryParser<GetAllUsersQuery, PagedList<UserDTO>>(alluserQuery));
        }

        [HttpPost("command/create")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "API")]
        [AuthorizePermission(Permission.CanManageUsers)]
        public async Task<IActionResult> CreateUser([FromBody]CreateUserCommand command)
        {
            return APIResponse(await WebApiCommandDispatcherAsync(command));
        }

        [HttpPatch("command/update")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "API")]
        [AuthorizePermission(Permission.CanManageUsers)]
        public async Task<IActionResult> UpdateUser([FromBody]UpdateUserCommand command)
        {
            command.CurrentUserId = (await GetCurrentUser()).Id;
            return APIResponse(await WebApiCommandDispatcherAsync(command));
        }

        [HttpPost("command/forgot-password-restore")]
        [AllowAnonymous]
        public async Task<IActionResult> ForgotPasswordRestore([FromBody] ForgotPasswordRestoreCommand command)
        {
            return APIResponse(await WebApiCommandDispatcherAsync(command));
        }

        [HttpPost("command/restore-password-by-code")]
        [AllowAnonymous]
        public async Task<IActionResult> RestorePasswordByCode([FromBody] RestorePasswordByCodeCommand command)
        {
            return APIResponse(await WebApiCommandDispatcherAsync(command));
        }
    }
}