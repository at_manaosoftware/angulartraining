using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularTraining.Core.CQS.Tenants.Commands;
using AngularTraining.Core.CQS.Tenants.Queries;
using AngularTraining.Core.DomainModels.Tenants;
using AngularTraining.Core.Helpers.Paginations;
using AngularTraining.WebAPI.Authentication;
using AngularTraining.WebAPI.Models.Shared;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StructureMap;
using static AngularTraining.WebAPI.Authentication.RolePermissionFilter;

namespace AngularTraining.WebAPI.Controllers.V1
{
    [Produces("application/json")]
    [Route("api/v1/tenant")]
    public class TenantController : BaseController
    {
        public TenantController(IContainer container) : base(container)
        {

        }

        [HttpGet("query/tenants")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "API")]
        [AuthorizePermission(Permission.CanReadTenants)]
        public async Task<IActionResult> GetAllTenants(GetAllTenantsQuery allTenantsQuery)
        {
            var results = await WebApiQueryParser<GetAllTenantsQuery, Task<PagedList<TenantDTO>>>(allTenantsQuery);
            return APIResponse(results);
        }

        [HttpGet("query/tenants/options")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "API")]
        [AuthorizePermission(Permission.CanReadTenants)]
        public async Task<IActionResult> GetAllTenantsAsOptions()
        {
            var results = await WebApiQueryParser<GetAllTenantOptionsQuery, Task<IList<TenantDTO>>>(new GetAllTenantOptionsQuery());
            return APIResponse(results.Select(x => new Option<Guid> { Text = x.Name, Value = x.Id}).ToList());
        }

        [HttpPost("command/create")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "API")]
        [AuthorizePermission(Permission.CanManageTenants)]
        public async Task<IActionResult> CreateTenant([FromBody]CreateNewTenantCommand command)
        {
            return APIResponse(await this.WebApiCommandDispatcherAsync(command));
        }
    }
}