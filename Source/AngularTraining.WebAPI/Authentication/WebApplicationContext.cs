using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using AngularTraining.Core.DomainModels.Identities;
using AngularTraining.Core.DomainModels.Users;
using AngularTraining.Core.Externals;
using AngularTraining.Core.Externals.Identities;
using AngularTraining.Core.Externals.Repositories;
using AngularTraining.Core.Helpers.Microsoft.DataTransfer.Basics;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTraining.WebAPI.Authentication
{
    public class WebApplicationContext : IApplicationContext
    {
        private IUnitOfWork unitOfWork;
        public WebApplicationContext(IHttpContextAccessor contextAccessor, IContainer container)
        {
            var context = contextAccessor.HttpContext;
            Guard.NotNull<HttpContext>("HttpContext", context);
            if (context.User.Identity.IsAuthenticated)
            {
                var DataToken = context.User.Claims.Select(x => x.Value).ToArray();
                string applicationUserId = DataToken[0];
                var userManager = container.GetInstance<IIdentityUserManager>();
                unitOfWork = container.GetInstance<IUnitOfWork>();
                this.CurrentApplicationUser = userManager.FindByIdAsync(applicationUserId).Result;
            }
            this.BaseUrl = $"{context.Request.Scheme}://{context.Request.Host}{context.Request.PathBase}";
        }

        public ApplicationUser CurrentApplicationUser { get; private set; }

        public string BaseUrl { get; private set; }
    }
}