using System;
using AngularTraining.Core.DomainModels.UserRoles;

namespace AngularTraining.WebAPI.Models.Users
{
    public class UserViewModel
    {
        public string Id { get; set; }

        public Guid? TenantId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public AbstractUserRole Permissions { get; set; }
    }
}