namespace AngularTraining.WebAPI.Models.Shared
{
    public class Option<T>
    {
        public string Text { get; set; }

        public T Value { get; set; }
    }
}