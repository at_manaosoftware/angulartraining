using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTraining.WebAPI.Helpers
{
    public class APIResponseWrapper
    {
        public object Data { get; set; }
        public IEnumerable<object> Errors { get; set; }
        public IEnumerable<object> Meta { get; set; }
    }
}