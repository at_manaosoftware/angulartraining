using AngularTraining.Core.Externals;
using AngularTraining.Infrastructure.IoC;
using AngularTraining.WebAPI.Authentication;
using StructureMap;

namespace AngularTraining.WebAPI.IoC
{
    public static class StructureMapContainerInit
    {
        public static IContainer InitializeContainer()
        {
            return new Container(c => c.AddRegistry<DefaultRegistry>());
        }
    }

    public class DefaultRegistry : StructureMapDefaultRegistry
    {
        #region Constructors and Destructors

        public DefaultRegistry() : base()
        {
            For<IApplicationContext>().Use<WebApplicationContext>();
        }

        #endregion
    }
}