using System.Threading.Tasks;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.Externals.Dispatcher;
using StructureMap;

namespace AngularTraining.Dispatcher.Dispatchers
{
    public class DefaultQueryParser : IQueryParser
    {
        private IContainer ioCContainer;

        public DefaultQueryParser(IContainer ioCContainer)
        {
            this.ioCContainer = ioCContainer;
        }

        public TResult Process<TResult>(IQuery<TResult> query)
        {
            try
            {
                var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));
                dynamic handler = ioCContainer.GetInstance(handlerType);

                if (handler == null)
                    throw new QueryHandlerNotFoundException(typeof(IQuery<TResult>));

                return handler.Handle((dynamic)query);
            }
            catch (StructureMapConfigurationException)
            {
                throw new QueryHandlerNotFoundException(typeof(IQuery<TResult>));
            }
        }
    }
}