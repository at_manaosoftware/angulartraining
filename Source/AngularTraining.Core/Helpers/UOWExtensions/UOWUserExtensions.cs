using Microsoft.EntityFrameworkCore;
using AngularTraining.Core.DomainModels.Users;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;

namespace AngularTraining.Core.Helpers.UOWExtensions
{
    public static class UOWUserExtensions
    {
        public static IEnumerable<UserDTO> GetUsersAsDTO(this IQueryable<User> dbSet)
        {
            return dbSet.Include(x => x.ApplicationUser).Select(x => x.ToUserDTO());
        }

        public static async Task<User> GetUserByAppIdIncludeAppUser(this DbSet<User> dbSet, string appUserId)
        {
            return await dbSet.Include(x => x.ApplicationUser).Where(x => x.ApplicationUserId == appUserId).SingleOrDefaultAsync();
        }
    }
}