using System.Collections.Generic;
using System.Linq;
using AngularTraining.Core.DomainModels.Tenants;
using Microsoft.EntityFrameworkCore;

namespace AngularTraining.Core.Helpers.UOWExtensions
{
    public static class UOWTenantExtensions
    {
        public static IEnumerable<TenantDTO> GetTenantsAsDTO(this DbSet<Tenant> dbSet)
        {
            return dbSet.Include(x => x.Users).Select(x => x.ToTenantDTO());
        }

        public static IEnumerable<TenantDTO> GetTenantsAsOptions(this DbSet<Tenant> dbSet)
        {
            return dbSet.Select(x => x.ToTenantDTO());
        }
    }
}