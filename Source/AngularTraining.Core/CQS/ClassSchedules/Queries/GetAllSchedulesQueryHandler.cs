using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.DomainModels.ClassSchedules;
using AngularTraining.Core.Externals.Repositories;
using AngularTraining.Core.Helpers.Microsoft.DataTransfer.Basics;
using AngularTraining.Core.Helpers.Paginations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AngularTraining.Core.CQS.ClassSchedules.Queries
{
    public class GetAllSchedulesQueryHandler : IQueryHandler<GetAllSchedulesQuery, PagedList<ClassScheduleDTO>>
    {
        private IUnitOfWork unitOfWork;
        public GetAllSchedulesQueryHandler(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public PagedList<ClassScheduleDTO> Handle(GetAllSchedulesQuery query)
        {
            Guard.NotNull("GetAllSchedulesQuery", query);
            var classScheduleQuery = unitOfWork.ClassScheduleDBSet.Select(o => o.ToClassScheduleDTO()).AsQueryable();
            return new PagedList<ClassScheduleDTO>(classScheduleQuery, query.Page, query.PageSize);

        }
    }
}