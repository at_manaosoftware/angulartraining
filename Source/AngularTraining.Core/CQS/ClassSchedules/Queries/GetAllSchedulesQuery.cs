using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.DomainModels.ClassSchedules;
using AngularTraining.Core.Helpers.Paginations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.CQS.ClassSchedules.Queries
{
    public class GetAllSchedulesQuery : AbstractPagingQuery<PagedList<ClassScheduleDTO>>
    {
    }
}