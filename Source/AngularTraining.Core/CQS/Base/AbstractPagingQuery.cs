using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.CQS.Base
{
    public class AbstractPagingQuery<TResult> : IQuery<TResult>
    {
        public string Search { get; set; }
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 20;
        public bool Ascending { get; set; } = true;
    }
}