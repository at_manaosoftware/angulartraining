using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.DomainModels.Tenants;
using AngularTraining.Core.Externals.Repositories;
using AngularTraining.Core.Helpers.Microsoft.DataTransfer.Basics;
using AngularTraining.Core.Helpers.UOWExtensions;
using Microsoft.EntityFrameworkCore;

namespace AngularTraining.Core.CQS.Tenants.Queries
{
    public class GetAllTenantOptionsQueryHandler : IQueryHandler<GetAllTenantOptionsQuery, Task<IList<TenantDTO>>>
    {
        private IUnitOfWork unitOfWork;

        public GetAllTenantOptionsQueryHandler(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public async Task<IList<TenantDTO>> Handle(GetAllTenantOptionsQuery query)
        {
            Guard.NotNull("GetAllTenantOptionsQuery", query);
            return await this.unitOfWork.TenantsDBSet.GetTenantsAsOptions().AsQueryable().ToListAsync();
        }

    }
}