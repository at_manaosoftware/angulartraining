using System.Collections.Generic;
using System.Threading.Tasks;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.DomainModels.Tenants;

namespace AngularTraining.Core.CQS.Tenants.Queries
{
    public class GetAllTenantOptionsQuery : IQuery<Task<IList<TenantDTO>>>
    {
    }
}