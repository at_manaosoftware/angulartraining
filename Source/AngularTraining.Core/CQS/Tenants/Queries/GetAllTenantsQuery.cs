using System.Threading.Tasks;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.DomainModels.Tenants;
using AngularTraining.Core.Helpers.Paginations;

namespace AngularTraining.Core.CQS.Tenants.Queries
{
    public class GetAllTenantsQuery : AbstractPagingQuery<Task<PagedList<TenantDTO>>>
    {
    }
}