using System.Linq;
using System.Threading.Tasks;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.DomainModels.Tenants;
using AngularTraining.Core.Externals.Repositories;
using AngularTraining.Core.Helpers.Microsoft.DataTransfer.Basics;
using AngularTraining.Core.Helpers.Paginations;
using AngularTraining.Core.Helpers.UOWExtensions;

namespace AngularTraining.Core.CQS.Tenants.Queries
{
    public class GetAllTenantsQueryHandler : IQueryHandler<GetAllTenantsQuery, Task<PagedList<TenantDTO>>>
    {
        private IUnitOfWork unitOfWork;

        public GetAllTenantsQueryHandler(IUnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public Task<PagedList<TenantDTO>> Handle(GetAllTenantsQuery query)
        {
            Guard.NotNull("GetAllTenantsQuery", query);
            var results = new PagedList<TenantDTO>(this.unitOfWork.TenantsDBSet.GetTenantsAsDTO().AsQueryable(), query.Page, query.PageSize);
            return Task.FromResult(results);
        }
    }
}