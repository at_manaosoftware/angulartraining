using AngularTraining.Core.CQS.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.CQS.Users.Commands
{
    public class ForgotPasswordRestoreCommand : ICommand
    {
        public string Email { get; set; }
    }
}