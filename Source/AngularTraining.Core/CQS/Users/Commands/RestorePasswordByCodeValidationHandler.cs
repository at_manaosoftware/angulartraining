using FluentValidation;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.CQS.Users.Commands
{
    public class RestorePasswordByCodeValidationHandler : AbstractValidationHandler<RestorePasswordByCodeCommand>
    {
        public RestorePasswordByCodeValidationHandler(LocalizationService localization)
        {
            {
                RuleFor(x => x.Email).EmailAddress().WithMessage(localization.GetLocalizedHtmlString("EMAIL_WRONGFORMAT_ERROR"));
                RuleFor(x => x.NewPassword).NotEmpty().WithMessage(string.Format(localization.GetLocalizedHtmlString("SHARED_FIELDREQUIREDERRORMESSAGE"), "NewPassword"));
                RuleFor(x => x.Code).NotEmpty().WithMessage(string.Format(localization.GetLocalizedHtmlString("SHARED_FIELDREQUIREDERRORMESSAGE"), "Code"));
            }
        }
    }

}