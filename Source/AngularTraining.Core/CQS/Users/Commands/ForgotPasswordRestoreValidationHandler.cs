using FluentValidation;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.Resources;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.CQS.Users.Commands
{
    public class ForgotPasswordRestoreValidationHandler : AbstractValidationHandler<ForgotPasswordRestoreCommand>
    {
        public ForgotPasswordRestoreValidationHandler(LocalizationService localization)
        {
            RuleFor(x => x.Email).NotEmpty().WithMessage(localization.GetLocalizedHtmlString("SHARED_FIELDREQUIREDERRORMESSAGE"));
        }
    }
}