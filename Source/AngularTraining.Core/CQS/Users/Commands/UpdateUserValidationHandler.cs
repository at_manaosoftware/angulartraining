using FluentValidation;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.Resources;

namespace AngularTraining.Core.CQS.Users
{
    public class UpdateUserValidationHandler : AbstractValidationHandler<UpdateUserCommand>
    {
        public UpdateUserValidationHandler(LocalizationService localization)
        {
            RuleFor(x => x.FirstName).NotEmpty().WithMessage(localization.GetLocalizedHtmlString("VALIDATION_FIRSTNAME_NOT_NULL"));
        }
    }
}