using AngularTraining.Core.CQS.Base;

namespace AngularTraining.Core.CQS.Users
{
    public class UpdateUserCommand : ICommand
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string CurrentUserId { get; set; }
    }
}