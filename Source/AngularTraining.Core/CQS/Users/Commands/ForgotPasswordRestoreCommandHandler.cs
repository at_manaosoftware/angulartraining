using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.Externals;
using AngularTraining.Core.Externals.Identities;
using AngularTraining.Core.Externals.Messages;
using AngularTraining.Core.Externals.Repositories;
using AngularTraining.Core.Resources;
using System;
using System.Threading.Tasks;

namespace AngularTraining.Core.CQS.Users.Commands
{
    public class ForgotPasswordRestoreCommandHandler : AbstractCommandHandler<ForgotPasswordRestoreCommand>
    {
        private IUnitOfWork unitOfWork;
        private IIdentityUserManager userManager;
        private IEmailSender emailSender;
        private LocalizationService localization;
        private IApplicationContext context;

        public ForgotPasswordRestoreCommandHandler(IUnitOfWork unitOfWork, IIdentityUserManager userManager, IEmailSender emailSender, LocalizationService localization, IApplicationContext context)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
            this.emailSender = emailSender;
            this.localization = localization;
            this.context = context;
        }

        public async override Task<CommandResult> ExecuteAsync(ForgotPasswordRestoreCommand command)
        {
            var user = await userManager.FindByEmailAsync(command.Email);

            if (user != null && await userManager.IsEmailConfirmedAsync(user))
            {
                var token = userManager.GeneratePasswordResetTokenAsync(user).Result;
                var emailSubject = localization.GetLocalizedHtmlString("EMAIL_RESET_PASSWORD_SUBJECT");
                
                // Replace the token with a link to a frontend page (use context.BaseUrl for getting the base url)
                var emailBody = String.Format(localization.GetLocalizedHtmlString("EMAIL_RESET_PASSWORD_BODY"), "use this token to test restore password:  "+token);
                await emailSender.SendEmailAsync(user.Email, emailSubject, emailBody);
            }

            return new CommandResult();
        }
    }
}