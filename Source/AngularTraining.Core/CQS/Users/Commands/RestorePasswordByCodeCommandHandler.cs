using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.Externals.Identities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AngularTraining.Core.CQS.Users.Commands
{
    public class RestorePasswordByCodeCommandHandler : AbstractCommandHandler<RestorePasswordByCodeCommand>
    {
        private IIdentityUserManager userManager;
        public RestorePasswordByCodeCommandHandler(IIdentityUserManager userManager)
        {
            this.userManager = userManager;
        }

        public async override Task<CommandResult> ExecuteAsync(RestorePasswordByCodeCommand command)
        {
            var user = await userManager.FindByEmailAsync(command.Email);
            var result = await userManager.ResetPasswordAsync(user, command.Code, command.NewPassword);
            return new CommandResult(result);
        }
    }
}