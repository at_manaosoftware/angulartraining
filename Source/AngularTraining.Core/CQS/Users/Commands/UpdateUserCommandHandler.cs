using System.Threading.Tasks;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.Externals.Identities;
using AngularTraining.Core.Externals.Repositories;
using AngularTraining.Core.Helpers.Microsoft.DataTransfer.Basics;
using Microsoft.EntityFrameworkCore;

namespace AngularTraining.Core.CQS.Users
{
    public class UpdateUserCommandHandler : AbstractCommandHandler<UpdateUserCommand>
    {
        private IUnitOfWork unitOfWork;
        private IIdentityUserManager userManager;

        public UpdateUserCommandHandler(IUnitOfWork unitOfWork, IIdentityUserManager userManager)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
        }

        public async override Task<CommandResult> ExecuteAsync(UpdateUserCommand command)
        {
            Guard.NotNull("UpdateUserCommand", command);

            var currentAppUser = await userManager.FindByIdAsync(command.CurrentUserId);
            if (currentAppUser == null)
                return new CommandResult(false, "PERMISSION_NOT_ALLOWED");

            var applicationUser = await userManager.FindByIdAsync(command.Id);
            if (applicationUser == null)
                return new CommandResult(false, "USER_NOT_FOUND");

            var currentUser = await unitOfWork.UsersDBSet.FirstOrDefaultAsync(x => x.ApplicationUserId == currentAppUser.Id);
            if (!currentAppUser.IsSuperAdministrator && currentUser == null)
                return new CommandResult(false, "PERMISSION_NOT_ALLOWED");

            var user = await unitOfWork.UsersDBSet.FirstOrDefaultAsync(x => x.ApplicationUserId == applicationUser.Id);
            if (!currentAppUser.IsSuperAdministrator && user == null)
                return new CommandResult(false, "USER_NOT_FOUND");

            var allowEdit = currentAppUser.IsSuperAdministrator ||
                (currentUser.UserRoleObject.CanManageTenants && currentUser.TenantId == user.TenantId) ||
                currentUser.Id == user.Id;

            if (!allowEdit)
                return new CommandResult(false, "PERMISSION_NOT_ALLOWED");

            if (string.IsNullOrEmpty(command.FirstName))
                return new CommandResult(false, "FIRST_NAME_REQUIRED");

            applicationUser.FirstName = command.FirstName;
            applicationUser.LastName = command.LastName;

            if (!string.IsNullOrEmpty(command.Password) && !string.IsNullOrEmpty(command.NewPassword))
            {
                if (!(await userManager.CheckPasswordAsync(applicationUser, command.Password)))
                    return new CommandResult(false, "INVALID_CURRENT_PASSWORD");

                await userManager.ResetPasswordAsync(
                    applicationUser,
                    await userManager.GeneratePasswordResetTokenAsync(applicationUser), 
                    command.NewPassword);
            }

            await unitOfWork.SaveChangesAsync();

            return new CommandResult(true);
        }
    }
}