using AngularTraining.Core.CQS.Base;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.CQS.Users.Commands
{
    public class RestorePasswordByCodeCommand : ICommand
    {
        public string Code { get; set; }
        public string Email { get; set; }
        public string NewPassword { get; set; }
    }
}