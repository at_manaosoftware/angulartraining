using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.DomainModels.Identities;
using AngularTraining.Core.Externals.Identities;
using AngularTraining.Core.Externals.Repositories;
using AngularTraining.Core.Helpers.Microsoft.DataTransfer.Basics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AngularTraining.Core.CQS.Users
{
    public class CreateUserCommandHandler : AbstractCommandHandler<CreateUserCommand>
    {
        private IUnitOfWork unitOfWork;
        private IIdentityUserManager userManager;

        public CreateUserCommandHandler(IUnitOfWork unitOfWork, IIdentityUserManager userManager)
        {
            this.unitOfWork = unitOfWork;
            this.userManager = userManager;
        }

        public async override Task<CommandResult> ExecuteAsync(CreateUserCommand command)
        {
            Guard.NotNull<CreateUserCommand>("CreateUserCommand", command);

            var applicationUser = new ApplicationUser
            {
                FirstName = command.FirstName,
                LastName = command.LastName,
                UserName = command.Username,
                Email = command.Username,
                EmailConfirmed = command.EmailConfirmed
            };

            var result = await userManager.CreateAsync(applicationUser, command.Password);
            if (result.Errors.Any())
            {
                var commandResult = new CommandResult(false, result.Errors.Select(x => x.Description));
                return commandResult;
            }
            await this.unitOfWork.UsersDBSet.AddAsync(new DomainModels.Users.User(applicationUser,command.TenantID));
            await this.unitOfWork.SaveChangesAsync();
            return await base.ExecuteAsync(command);
        }
    }
}