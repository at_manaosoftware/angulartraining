using AngularTraining.Core.CQS.Users.Queries;
using AngularTraining.Core.DomainModels.Users;
using System;
using System.Collections.Generic;
using System.Text;
using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.Externals.Repositories;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using AngularTraining.Core.Helpers.UOWExtensions;
using AngularTraining.Core.Helpers.Microsoft.DataTransfer.Basics;
using AngularTraining.Core.Externals;
using AngularTraining.Core.Helpers.Paginations;

namespace AngularTraining.Core.CQS.Users.QueryHandlers
{
    public class GetAllUsersQueryHandler : IQueryHandler<GetAllUsersQuery, PagedList<UserDTO>>
    {
        private IUnitOfWork unitOfWork;
        private IApplicationContext applicationContext;

        public GetAllUsersQueryHandler(IUnitOfWork unitOfWork, IApplicationContext applicationContext)
        {
            this.unitOfWork = unitOfWork;
            this.applicationContext = applicationContext;
        }

        public PagedList<UserDTO> Handle(GetAllUsersQuery query)
        {
            Guard.NotNull<GetAllUsersQuery>("GetAllUsersQuery", query);

            // Just and example of how to use the IApplicationContext.
            var currentUser = applicationContext.CurrentApplicationUser;
            var user = currentUser.Users.FirstOrDefault();
           
            if (!currentUser.IsSuperAdministrator && (user == null || user.UserRole != UserRole.TenantAdministrator))
            {
                throw new Exception("PERMISSION_DENIED");
            }

            var userQuery = unitOfWork.UsersDBSet.AsQueryable();

            if (!currentUser.IsSuperAdministrator)
            {
                userQuery = userQuery.Where(x => x.TenantId == user.TenantId);
            }

            return new PagedList<UserDTO>(userQuery.GetUsersAsDTO().AsQueryable(), query.Page, query.PageSize);
        }
    }
}