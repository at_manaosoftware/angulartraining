using AngularTraining.Core.CQS.Base;
using AngularTraining.Core.DomainModels.Users;
using AngularTraining.Core.Helpers.Paginations;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.CQS.Users.Queries
{
    public class GetAllUsersQuery : AbstractPagingQuery<PagedList<UserDTO>>
    { 

    }
}