using AngularTraining.Core.CQS.Base;

namespace AngularTraining.Core.Externals.Dispatcher
{
    public interface IQueryParser
    {
        TResult Process<TResult>(IQuery<TResult> query);
    }
}