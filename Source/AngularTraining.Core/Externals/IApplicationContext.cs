using AngularTraining.Core.DomainModels.Identities;
using AngularTraining.Core.DomainModels.Tenants;
using AngularTraining.Core.DomainModels.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.Externals
{
    public interface IApplicationContext
    {
        ApplicationUser CurrentApplicationUser { get; }
        String BaseUrl { get; }
    }
}