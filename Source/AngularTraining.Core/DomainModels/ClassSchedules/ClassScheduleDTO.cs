using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.DomainModels.ClassSchedules
{
    public class ClassScheduleDTO
    {
        public string RoomNumber { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int Lecture { get; set; }
        public int Lab { get; set; }
        public string Subject { get; set; }
        public DateTime MidtermExam { get; set; }
        public DateTime FinalExam { get; set; }
        public int Credit { get; set; }
    }
}