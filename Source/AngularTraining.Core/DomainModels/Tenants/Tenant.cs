using AngularTraining.Core.DomainModels.Base;
using AngularTraining.Core.DomainModels.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.DomainModels.Tenants
{
    public class Tenant : EntityBase<Guid>
    {
        public string Name { get; set; }
        public ICollection<User> Users { get; set; } = new List<User>();

        public TenantDTO ToTenantDTO()
        {
            return new TenantDTO()
            {
                Id = this.Id,
                Name = this.Name,
                UsersCount = this.Users.Count
            };
        }
    }
}