using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.DomainModels.Tenants
{
    public class TenantDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public int UsersCount { get; set; }
    }
}