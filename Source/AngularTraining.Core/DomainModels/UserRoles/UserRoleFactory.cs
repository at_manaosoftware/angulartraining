using AngularTraining.Core.DomainModels.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.DomainModels.UserRoles
{
    public static class UserRoleFactory
    {
        public static AbstractUserRole GetRoleFromRoleUser(UserRole userRole)
        {
            if (userRole == UserRole.TenantAdministrator)
                return new TenantAdministrator();
            else if (userRole == UserRole.TenantUser)
                return new TenantUser();
            else
                return null;
        }
    }
}