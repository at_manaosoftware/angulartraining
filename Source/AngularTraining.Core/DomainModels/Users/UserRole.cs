using System;
using System.Collections.Generic;
using System.Text;

namespace AngularTraining.Core.DomainModels.Users
{
    public enum UserRole
    {
        TenantAdministrator = 0,
        TenantUser = 1
    }
}