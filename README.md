# Manao Web Template

* ASP.NET Core 2.2
* Angular 7.2

Hosted at http://webtemplate.manaoweb/

Database can be accessed with `Server=manaodb;Database=Manao.Template.Web;Trusted_Connection=Yes;`

Sonar Qube can be accessed at http://manaosonarqube.company.manaosoftware.com:9000/dashboard?id=ManaoWebTemplate

TeamCity CI/CD http://manaoteamcity2:8089/project.html?projectId=ManaoWebTemplate&tab=projectOverview